package net.floodlightcontroller.loadbalancing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.projectfloodlight.openflow.protocol.OFMessage;
import org.projectfloodlight.openflow.protocol.OFType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.internal.ControllerCounters;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.debugcounter.IDebugCounterService;
import net.floodlightcontroller.threadpool.IThreadPoolService;

public class LoadBalancing implements IFloodlightModule, IOFMessageListener {
	
	
	// Default declaration
	protected IFloodlightProviderService floodlightProvider;
	protected static Logger logger;
    protected static IThreadPoolService threadPoolService;
    protected static IDebugCounterService debugCounterService;
	
    //Declare for Zookeeper 
    private static ZooKeeper zk;
    private static ZooKeeperConnection conn;
    
    /* Thread to print Counter */
    private static ScheduledFuture<?> printCounter;
    private static ScheduledFuture<?> getC2Statistics;
    private static ScheduledFuture<?> getC3Statistics;
    private static ScheduledFuture<?> getC4Statistics;
    private static ScheduledFuture<?> getC1Statistics;
    /* Thread to get amount of packetin */
    private static ScheduledFuture<?> getAmount;
    protected static ControllerCounters counters;
    
    private static int getAmountInterval =14;
    private static long pLC1;
    private static long pLC2;
    private static long pLC3;
    private static long pLC4;
    private static int MAXC1=120294;
    private static int MAXC2=118662;
    private static int MAXC3=119262;
    private static int MAXC4=117362;
    
    private static final String C1_IP = "10.102.11.249:8080";
    private static final String C2_IP = "10.102.11.247:8080";
    private static final String C3_IP = "10.102.11.224:8080";
    private static final String C4_IP = "10.102.11.225:8080";
    
    /* Variable for Print Counter threading*/
	private static String str="" ;
	private static long cLC1,cLC2,cLC3,cLC4;
	private static float prC1,prC2,prC3,prC4;
	private static float deltaLC1,deltaLC2,deltaLC3,deltaLC4;
//	Theses variables are used for retrive data by JSON.     
//  static int s1Counter;
//  static int s2Counter;
//  static int s3Counter;
//  static int s4counter;
//  static int s5counter;
//  static int s6counter;
//  static float pLC1;
//  static float pLC2;
    
    protected class GetC2Statistics implements Runnable {
    	public void run() {			
    		//getNodeData();
			String messC2 = getDataFromZK ("C2Znode","Mess");
			//System.out.println(messC2); 
			
			//Handle String with format like this [S3=5, S2=4, S1=1, cLC1=10] [S1=0, S2=0, S3=0, cLC1=0]]
			String [] setC2= messC2.replaceAll("[\\[\\]\\s]", "").split(","); 
			
 			cLC2 = Long.parseLong(setC2[3].substring(5)); 
			str="The number of C2 at "+java.time.LocalTime.now()+" is: " + cLC2;    			
			System.out.println(str); 
			prC2 = (float)cLC2/(MAXC2);
			System.out.println("Pressure of C2 " + prC2);
			deltaLC2= Math.abs(cLC2 -pLC2);   	
			//notify();
    	}
    }
  
    protected class GetC3Statistics implements Runnable {
    	public void run() {			
			String messC3 = getDataFromZK ("C3Znode","Mess");
			//System.out.println(messC3); 
			//Handle String with format like this [cLC1=10, S3=5, S2=4, S1=1]
			String [] setC3= messC3.replaceAll("[\\[\\]\\s]", "").split(",");     		   			
 			//cLC3 = Long.parseLong(messC3.substring(6,messC3.indexOf(","))); 
			cLC3 = Long.parseLong(setC3[3].substring(5)); 
			str="The number of C3 at "+java.time.LocalTime.now()+" is: " + cLC3;    			
			System.out.println(str); 
			prC3 = (float)cLC3/(MAXC3);
			System.out.println("Pressure of C3 " + prC3);
			deltaLC3= Math.abs(cLC3 -pLC3);	
			//notify();
    	}
    }
    protected class GetC4Statistics implements Runnable {
    	public void run() {			
			String messC4 = getDataFromZK ("C4Znode","Mess");
			//System.out.println(messC4); 
			//Handle String with format like this [cLC1=10, S3=5, S2=4, S1=1]
			String [] setC4= messC4.replaceAll("[\\[\\]\\s]", "").split(",");     		   			
 			//cLC4 = Long.parseLong(messC4.substring(6,messC4.indexOf(","))); 
			cLC4 = Long.parseLong(setC4[3].substring(5)); 
			str="The number of C4 at "+java.time.LocalTime.now()+" is: " + cLC4;    			
			System.out.println(str); 
			prC4 = (float)cLC4/(MAXC4);
			System.out.println("Pressure of C4 " + prC4);
			deltaLC4= Math.abs(cLC4 -pLC4);
			//notify();
    	}
    }    
    protected class GetC1Statistics implements Runnable {
    	public void run() {		
    		
			Map <String,Long> setC1= new HashMap<>();
			long cS1Counter=counters.packetInS1.getCounterValue();
			long cS2Counter= counters.packetInS2.getCounterValue();
			long cS3Counter= counters.packetInS3.getCounterValue();
			counters.packetInS1.reset();
			counters.packetInS2.reset();
			counters.packetInS3.reset(); 
			cLC1 = cS1Counter + cS2Counter + cS3Counter;
			str="The number of C1 at  "+java.time.LocalTime.now()+" is: " + cLC1;
			System.out.println(str); 
			setC1.put("S1", cS1Counter);
			setC1.put("S2", cS2Counter);
			setC1.put("S3", cS3Counter);
			setC1.put("cLC1", cLC1);   			  		   			   			
			prC1 = (float)cLC1/(MAXC1);
			System.out.println("Pressure of C1 " + prC1);
			deltaLC1= Math.abs( cLC1 -pLC1);	
			//notify();
			
    	}
    } 
    protected class PrintCounter implements Runnable {
    	
    	public void run () {
    		
			//if ((getC1Statistics.isDone() && getC2Statistics.isDone() && getC3Statistics.isDone() && getC4Statistics.isDone())) {
	    		try {
	    			System.out.println("Started at"+ java.time.LocalTime.now());	
	    			
	    			//Load of C2
	    			String messC2 = getDataFromZK ("C2Znode","Mess");
	    			//System.out.println(messC2); 
	    			
	    			//Handle String with format like this [S3=5, S2=4, S1=1, cLC1=10] [S1=0, S2=0, S3=0, cLC1=0]]
	    			String [] setC2= messC2.replaceAll("[\\[\\]\\s]", "").split(","); 
	    			
	     			cLC2 = Long.parseLong(setC2[3].substring(5)); 
	    			str="The number of C2 at "+java.time.LocalTime.now()+" is: " + cLC2 + " Load is: "+ (float)cLC2/getAmountInterval+ " MAX switch: "+ setC2[2];    			
	    			System.out.println(str); 
	    			prC2 = ((float)cLC2/getAmountInterval)/MAXC2;
	    			System.out.println("Pressure of C2 " + prC2);
	    			deltaLC2= Math.abs(cLC2 -pLC2); 
	    			
	    			//Load of C3
	    			String messC3 = getDataFromZK ("C3Znode","Mess");
	    			//System.out.println(messC3); 
	    			//Handle String with format like this [cLC1=10, S3=5, S2=4, S1=1]
	    			String [] setC3= messC3.replaceAll("[\\[\\]\\s]", "").split(",");     		   			
	     			//cLC3 = Long.parseLong(messC3.substring(6,messC3.indexOf(","))); 
	    			cLC3 = Long.parseLong(setC3[3].substring(5)); 
	    			str="The number of C3 at "+java.time.LocalTime.now()+" is: " + cLC3 + " Load is: "+ (float)cLC3/getAmountInterval + " MAX switch: "+ setC3[2];    			
	    			System.out.println(str); 
	    			prC3 = ((float)cLC3/getAmountInterval)/MAXC3;
	    			System.out.println("Pressure of C3 " + prC3);
	    			deltaLC3= Math.abs(cLC3 -pLC3);	
	    			
	    			//Load of C4
	    			String messC4 = getDataFromZK ("C4Znode","Mess");
	    			//System.out.println(messC4); 
	    			//Handle String with format like this [cLC1=10, S3=5, S2=4, S1=1]
	    			String [] setC4= messC4.replaceAll("[\\[\\]\\s]", "").split(",");     		   			
	     			//cLC4 = Long.parseLong(messC4.substring(6,messC4.indexOf(","))); 
	    			cLC4 = Long.parseLong(setC4[3].substring(5)); 
	    			str="The number of C4 at "+java.time.LocalTime.now()+" is: " + cLC4 + " Load is: "+ (float)cLC4/getAmountInterval + " MAX switch: "+ setC4[2];    			
	    			System.out.println(str); 
	    			prC4 = ((float)cLC4/getAmountInterval)/MAXC4;
	    			System.out.println("Pressure of C4 " + prC4);
	    			deltaLC4= Math.abs(cLC4 -pLC4);
	    			
	    			Comparator c=Collections.reverseOrder();
	    			// Load of C1
	    			Map <String,Long> setC1= new HashMap<>();
	    			long cS1Counter=counters.packetInS1.getCounterValue();
	    			long cS2Counter= counters.packetInS2.getCounterValue();
	    			long cS3Counter= counters.packetInS3.getCounterValue();
	    			counters.packetInS1.reset();
	    			counters.packetInS2.reset();
	    			counters.packetInS3.reset(); 
	    			cLC1 = cS1Counter + cS2Counter + cS3Counter;
	    			 
	    			setC1.put("S1", cS1Counter);
	    			setC1.put("S2", cS2Counter);
	    			setC1.put("S3", cS3Counter);	    			
	    			setC1.put("cLC1", cLC1); 
	    			List<Map.Entry<String, Long>> listC1=sortSetToList(setC1);
	    			str="The number of C1 at  "+java.time.LocalTime.now()+" is: " + cLC1 + " Load is: "+ (float)cLC1/getAmountInterval + " MAX switch: "+ listC1.get(2);
	    			System.out.println(str);
	    			
	    			
	    			prC1 = (((float)cLC1)/getAmountInterval)/MAXC1;
	    			System.out.println("Pressure of C1 " + prC1);
	    			deltaLC1= Math.abs( cLC1 -pLC1);
	    			
	    			// Total Load of controllers
	    			float totalLoad=cLC1+cLC2+cLC3+cLC4;
	    			
	    			// Calculate global imbalanced degree 
	    			float IB= (totalLoad)/(4* Math.max(Math.max(cLC1, cLC2),Math.max(cLC3, cLC4)));
	    			System.out.println("Imbalance degree: " + IB);
	    			
	    			//Calculate the average of Controller load
	    			float avgC=totalLoad/4;
	    			System.out.println("The average of Controller: " + avgC + " AVG/s : "+ avgC/getAmountInterval);
	    			
	    			//The difference between current load vs average load 
	    			float deltaAvgC1 =cLC1 -avgC;
	    			float deltaAvgC2 = cLC2 - avgC;
	    			float deltaAvgC3 = cLC3 - avgC;
	    			float deltaAvgC4 = cLC4 - avgC;
	    			System.out.println("Delta average: " + deltaAvgC1/getAmountInterval + " ; "+ deltaAvgC2/getAmountInterval + " ; "+ deltaAvgC3/getAmountInterval + " ; "+ deltaAvgC4/getAmountInterval);
	    			
	    			//Calculate the average of load fluctuation
	    			float AF = (deltaLC1+deltaLC2+deltaLC3+deltaLC4)/4;
	    			System.out.println("Average of load fluctuation: " + AF);
	    			
	    			
	    			// Save the previous value of LOAD
	    			pLC1=cLC1;
	    			pLC2=cLC2;
	    			pLC3=cLC3;
	    			pLC4=cLC4;
	    			
	    			//Change role
	    			
	    			float maxPrC=Math.max(Math.max(prC1, prC2), Math.max(prC3, prC4)); 
	    			float CThresshold=avgC+1;
	    			if (IB < 0.58 && avgC/getAmountInterval >=417) {
	    				String maxSwitch;
	    				String targetController;
		    			if (prC1==maxPrC) {
		    				
		    				float minDeltaAvg=Math.min(Math.min(deltaAvgC2, deltaAvgC3), deltaAvgC4);
		    				maxSwitch=nameToDPID(listC1.get(2).getKey());
		    				if (deltaAvgC2==minDeltaAvg) {
		    					targetController="C2";
		    					changeRole("10.102.11.247:8080",maxSwitch,"EQUAL");

		    				}else if (deltaAvgC3==minDeltaAvg) {
		    					targetController="C3";
		    					changeRole("10.102.11.225:8080",maxSwitch,"EQUAL");
		    				}else {
		    					targetController="C4";
		    					changeRole("10.102.11.224:8080",maxSwitch,"EQUAL");
		    				}
		    				//Assume that switch 1 is over	    				    			
		    				changeRole("10.102.11.249:8080",maxSwitch,"EQUAL");					
	  						sendDataToZK("C1","Command",listC1.get(2).getKey().toLowerCase()+";"+targetController);
	  						
	  						//String checkMigration=getDataFromZKWatcher("mininet","check");
	  						String checkMigration ="";
	  						do {
	  							checkMigration=getDataFromZK("mininet","check");
	  						} while (!checkMigration.equals("DONE"));
	  						
		                 	changeRole("10.102.11.249:8080",maxSwitch,"SLAVE");
		                 	switch (targetController) {
		                 		case "C2": 
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C3":
		                 			changeRole("10.102.11.225:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C4":
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 		default: 
		                 			System.out.println("ERROR!");
		                 			break;
		                 	}
		  					
	  						

		    				
		    			}else if (prC2==maxPrC) {
		    				float minDeltaAvg=Math.min(Math.min(deltaAvgC1, deltaAvgC3), deltaAvgC4);
		    				maxSwitch=nameToDPID(setC2[2]);
		    				if (deltaAvgC1==minDeltaAvg) {
		    					targetController="C1";
		    					changeRole("10.102.11.249:8080",maxSwitch,"EQUAL");

		    				}else if (deltaAvgC3==minDeltaAvg) {
		    					targetController="C3";
		    					changeRole("10.102.11.225:8080",maxSwitch,"EQUAL");
		    				}else {
		    					targetController="C4";
		    					changeRole("10.102.11.224:8080",maxSwitch,"EQUAL");
		    				}
		    				//Assume that switch 1 is over	    				    			
		    				changeRole("10.102.11.247:8080",maxSwitch,"EQUAL");					
	  						sendDataToZK("C1","Command",setC2[2].toLowerCase()+";"+targetController);
	  						
	  						//String checkMigration=getDataFromZKWatcher("mininet","check");
	  						String checkMigration ="";
	  						do {
	  							checkMigration=getDataFromZK("mininet","check");
	  						} while (!checkMigration.equals("DONE"));
	  						
		                 	changeRole("10.102.11.247:8080",maxSwitch,"SLAVE");
		                 	switch (targetController) {
		                 		case "C1": 
		                 			changeRole("10.102.11.249:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C3":
		                 			changeRole("10.102.11.225:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C4":
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 		default: 
		                 			System.out.println("ERROR!");
		                 			break;
		                 	}
		    			}else if (prC3==maxPrC) {
		    				float minDeltaAvg=Math.min(Math.min(deltaAvgC2, deltaAvgC1), deltaAvgC4);
		    				maxSwitch=nameToDPID(setC3[2]);
		    				if (deltaAvgC2==minDeltaAvg) {
		    					targetController="C2";
		    					changeRole("10.102.11.247:8080",maxSwitch,"EQUAL");

		    				}else if (deltaAvgC1==minDeltaAvg) {
		    					targetController="C1";
		    					changeRole("10.102.11.249:8080",maxSwitch,"EQUAL");
		    				}else {
		    					targetController="C4";
		    					changeRole("10.102.11.224:8080",maxSwitch,"EQUAL");
		    				}
		    				//Assume that switch 1 is over	    				    			
		    				changeRole("10.102.11.225:8080",maxSwitch,"EQUAL");					
	  						sendDataToZK("C1","Command",setC3[2].toLowerCase()+";"+targetController);
	  						
	  						//String checkMigration=getDataFromZKWatcher("mininet","check");
	  						String checkMigration ="";
	  						do {
	  							checkMigration=getDataFromZK("mininet","check");
	  						} while (!checkMigration.equals("DONE"));
	  						
		                 	changeRole("10.102.11.225:8080",maxSwitch,"SLAVE");
		                 	switch (targetController) {
		                 		case "C2": 
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C1":
		                 			changeRole("10.102.11.249:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C4":
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 		default: 
		                 			System.out.println("ERROR!");
		                 			break;
		                 	}
		    			}else {
		    				float minDeltaAvg=Math.min(Math.min(deltaAvgC2, deltaAvgC3), deltaAvgC1);
		    				maxSwitch=nameToDPID(setC4[2]);
		    				if (deltaAvgC4==minDeltaAvg) {
		    					targetController="C2";
		    					changeRole("10.102.11.247:8080",maxSwitch,"EQUAL");

		    				}else if (deltaAvgC3==minDeltaAvg) {
		    					targetController="C3";
		    					changeRole("10.102.11.225:8080",maxSwitch,"EQUAL");
		    				}else {
		    					targetController="C1";
		    					changeRole("10.102.11.224:8080",maxSwitch,"EQUAL");
		    				}
		    				//Assume that switch 1 is over	    				    			
		    				changeRole("10.102.11.224:8080",maxSwitch,"EQUAL");					
	  						sendDataToZK("C1","Command",setC4[2].toLowerCase()+";"+targetController);
	  						
	  						//String checkMigration=getDataFromZKWatcher("mininet","check");
	  						String checkMigration ="";
	  						do {
	  							checkMigration=getDataFromZK("mininet","check");
	  						} while (!checkMigration.equals("DONE"));
	  						
		                 	changeRole("10.102.11.224:8080",maxSwitch,"SLAVE");
		                 	switch (targetController) {
		                 		case "C2": 
		                 			changeRole("10.102.11.247:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C3":
		                 			changeRole("10.102.11.225:8080",maxSwitch,"MASTER");
		                 			break;
		                 		case "C1":
		                 			changeRole("10.102.11.249:8080",maxSwitch,"MASTER");
		                 		default: 
		                 			System.out.println("ERROR!");
		                 			break;
		                 	}
		    			}
	    			}
	    			
	   			    			
	    			System.out.println("Ended at"+ java.time.LocalTime.now());
	    		}catch (Exception ex) {
	    			System.out.println("FAILED");
	    		}
			}
        	//counters.packetIn.reset();
    	//}
    }



	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		// TODO Auto-generated method stub
	    floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);	
	    threadPoolService = context.getServiceImpl(IThreadPoolService.class);
	    debugCounterService = context.getServiceImpl(IDebugCounterService.class);
	    counters= new ControllerCounters(debugCounterService);
	    logger = LoggerFactory.getLogger(LoadBalancing.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
		// TODO Auto-generated method stub
		floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
		try {
			conn = new ZooKeeperConnection();
			zk = conn.connect("10.102.11.226");
			System.out.println("Connected to Zookeeper Node");
		}catch (Exception ex) {
			System.out.println("Error when connecting to Node"+ ex);
		}
       
        //getC2Statistics= threadPoolService.getScheduledExecutor().scheduleAtFixedRate(new GetC2Statistics(),15000,getAmountInterval, TimeUnit.MILLISECONDS);
        //getC3Statistics= threadPoolService.getScheduledExecutor().scheduleAtFixedRate(new GetC3Statistics(),15000,getAmountInterval, TimeUnit.MILLISECONDS);
        //getC4Statistics= threadPoolService.getScheduledExecutor().scheduleAtFixedRate(new GetC4Statistics(),15000,getAmountInterval, TimeUnit.MILLISECONDS);
        //getC1Statistics= threadPoolService.getScheduledExecutor().scheduleAtFixedRate(new GetC1Statistics(),15000,getAmountInterval, TimeUnit.MILLISECONDS);
        printCounter=threadPoolService.getScheduledExecutor().scheduleAtFixedRate(new PrintCounter(),17,getAmountInterval, TimeUnit.SECONDS);
	}

	   public static void create(String path, byte[] data) throws 
	      KeeperException,InterruptedException {
	      zk.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE,
	      CreateMode.PERSISTENT);
	   }
	   
    public static void update(String path, byte[] data) throws
    KeeperException,InterruptedException {
    	zk.setData(path, data, zk.exists(path,true).getVersion());
	}
    
    public static Stat znode_exists(String path) throws KeeperException,InterruptedException {
	    return zk.exists(path,true);
    }
    /**
     * Zookeeper is a third party used for creating a communication channel between controllers.
     * Every controller has 2 functions which are responsible for sending Message to ZK and receiving from that.
     *  
    */


    
	public void sendDataToZK (String contrl,String aim, String mess) {
	
	    String path= "/" +contrl+ "Znode/"+ aim;
	    byte[] data = mess.getBytes(); //Assign data which is to be updated.
			
	    try {
	       //create(path,data);
	       update(path, data); // Update znode data to the specified path
	       System.out.println("DOne!");
	       //conn.close();
	    } catch(Exception e) {
	    	
	       System.out.println("FAILED "+e.getMessage());
	    }
	}
    public String getDataFromZK(String contrl,String aim) {
        String path = "/" + contrl +"/" + aim;
        //final CountDownLatch connectedSignal = new CountDownLatch(1);
  		
  		String data;
        try {
            //conn = new ZooKeeperConnection();
           // zk = conn.connect("10.102.11.226");
           Stat stat = znode_exists(path);
  			
           if(stat != null) {
              byte[] b = zk.getData(path, true, null);
  				
              data = new String(b, "UTF-8");             
              //connectedSignal.await();          
              return data;
  				
           } else {
              System.out.println("Node does not exists");
              return null;
           }
        } catch(Exception e) {
          System.out.println(e.getMessage());
          return null;
        }    	
    }	

    public String getDataFromZKWatcher(String contrl,String aim) {
        String path = "/" + contrl +"/" + aim;
        final CountDownLatch connectedSignal = new CountDownLatch(1);
  		
        try {
//           conn = new ZooKeeperConnection();
//           zk = conn.connect("10.102.11.226");
           Stat stat = znode_exists(path);
  			
           if(stat != null) {
              byte[] b = zk.getData(path, new Watcher() {
  				
                 public void process(WatchedEvent we) {
  					
                    if (we.getType() == Event.EventType.None) {
                       switch(we.getState()) {
                          case Expired:
                          connectedSignal.countDown();
                          break;
                       }
  							
                    } else {
                       String path = "/" + contrl +"/" + aim;
  							
                       try {
                          byte[] bn = zk.getData(path,
                          false, null);
                          String data = new String(bn,
                          "UTF-8");
                          System.out.println(data);
                          connectedSignal.countDown();
  							
                       } catch(Exception ex) {
                          System.out.println(ex.getMessage());
                       }
                    }
                 }
              }, null);
  				
              String data = new String(b, "UTF-8");
              //System.out.println(data);
              connectedSignal.await();
              return data;
  				
           } else {
              System.out.println("Node does not exists");
              return null;
           }
        } catch(Exception e) {
          System.out.println(e.getMessage());
          return null;
        }    	
    }
    /*
     * Arrange the Set in ascending order
     * */
    public List<Map.Entry<String, Long>> sortSetToList(Map<String,Long> set ){
	    List<Map.Entry<String, Long>> listC2 = new ArrayList<>(set.entrySet());
	    
	    // Create a comparator to sort by value
	    Comparator<Entry<String, Long>> valueComparator = new Comparator<Entry<String, Long>>() {
	        @Override
	        public int compare(Entry<String, Long> o1, Entry<String, Long> o2) {
	            return o1.getValue().compareTo(o2.getValue());
	        }
	    };
	
	    // Sorting a List
	    
	    Collections.sort(listC2, valueComparator);
	    return listC2;
    }
    /*
     * Transfer S1 to DPID
     * 
     * */
    public String nameToDPID (String str) {
    	switch (str) {
    		case "S1":
    			return "00:00:00:00:00:00:00:01";
    		case "S2": 
    			return "00:00:00:00:00:00:00:02";
    		case "S3":
    			return "00:00:00:00:00:00:00:03";
    		case "S4":
    			return "00:00:00:00:00:00:00:04";
    		case "S5":
    			return "00:00:00:00:00:00:00:05";
    		case "S6":
    			return "00:00:00:00:00:00:00:06";
    		case "S7":
    			return "00:00:00:00:00:00:00:07";
    		case "S8" :
    			return "00:00:00:00:00:00:00:08";
    		case "S9" :
    			return "00:00:00:00:00:00:00:09";
    		case "S10": 
    			return "00:00:00:00:00:00:00:0a";
    		case "S11":
    			return "00:00:00:00:00:00:00:0b";
    		case "S12":
    			return "00:00:00:00:00:00:00:0c";
    		default:
    			return null;	
    	}
    }

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return LoadBalancing.class.getSimpleName();
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
		// TODO Auto-generated method stub
//		switch(msg.getType()) {
//			case PACKET_IN:
//			//System.out.println("From MACTracker" + msg);
//				count++;
//				System.out.println("at LB "+java.time.LocalTime.now()+ "is: "+ count);
//			//buffer.add(new SwitchMessagePair(sw, msg));
//				break;
//			default:
//				break;
//		}
		return Command.CONTINUE;
	}
	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		// TODO Auto-generated method stub
	    Collection<Class<? extends IFloodlightService>> l =
	            new ArrayList<Class<? extends IFloodlightService>>();
	        l.add(IFloodlightProviderService.class);
	        l.add(IThreadPoolService.class);
	        
	        return l;
	}

	
	/*
	 * Test get Node Data 
	 * Its default  function on the tutorial site can't get the value from Node if another controller don't connect to 
	 * The error is caused by "watcher()" function in getData, specifically 
	 * The getData can retrieve data and print it out but cannot return *still now know why*
	 * 
	 * The testing function below is using CuratorFramework but it doesn't work well. It got some errors when connecting to Node
	 * */
    public void getNodeData() {
    	System.out.println("Vo r ");
    	try {
    	    byte[] temp=zk.getData("/FirstZnode", true, zk.exists("/FirstZnode", true));
    	    try {
				String data = new String(temp, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	    System.out.println("Vo r 1"+ temp);
    	} catch(InterruptedException intrEx) {
    	    // do something to prevent the program from crashing
    	    System.out.println("\"new_znodet exist!");
    	} catch(KeeperException kpEx) {
    	    // do something to prevent the program from crashing
    	    System.out.println("\"new_znodet exist!");
    	}
//    	CuratorFramework zkClient = CuratorFrameworkFactory.builder().connectString("10.102.11.226:2181").build();
//    	zkClient.start();
//    	System.out.println("Vo r 1");
//    	boolean isExist = false;
//		try {
//			isExist = zkClient.checkExists().forPath("/FirstZnode") != null;
//			System.out.println("Vo r 2");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	if (isExist) {
//    		System.out.println("Vo r 3");
//    	    byte[] myNodeBytes = null;
//			try {
//				myNodeBytes = zkClient.getData().forPath("/FirstZnode");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    	    if (myNodeBytes != null && myNodeBytes.length > 0) {
//    	    	System.out.println("Vo r ");
//    	        System.out.println(new String(myNodeBytes));
//    	    }
//    	}
//    	else {
//    	    try {
//				zkClient.create().withMode(CreateMode.PERSISTENT).forPath("/FirstZnode", "foo".getBytes());
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    	}
    }
	
    //Define thread to calculate the values in formulas
    protected class GetAmount implements Runnable {
    	public void run () {
    		try {   	
    			
//            	String str1="The number of packetin at "+java.time.LocalTime.now()+" is: "+ counters.packetIn.getCounterValue();
//            	System.out.println(str1);
//            	counters.packetIn.reset();
//    			String str="";
//    			//System.out.println(s1counter);
//    			//s1counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:01");
//    			int deltaS1=Math.abs((getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:01") - s1counter));
//				str = " AT " + java.time.LocalTime.now() + " S1: " + deltaS1 ;
//				//str1 = " AT " + java.time.LocalTime.now() + "by its own S1: " + counters.packetInS1.getCounterValue();
//				//s1counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:01");
//				System.out.println(str);
//				//System.out.println(str1);
//				//System.out.println(s1counter);
//				//int s2counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:02");
//				int deltaS2=Math.abs((getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:02") - s2counter));
//				str = " AT " + java.time.LocalTime.now() + " S2: " + deltaS2;
//				System.out.println(str);
//				int deltaS3=Math.abs((getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:03") - s3counter));
//				//int s3counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:03");
//				str = " AT " + java.time.LocalTime.now() + " S3: " + deltaS3;
//				System.out.println(str);
//				
//				float cLC1=deltaS1 + deltaS2 + deltaS3;
//				System.out.println("Total packetin of C1: "+  cLC1);
//				int deltaS4=Math.abs((getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:04") - s4counter));
//				//int s4counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:04");
//				str = " AT " + java.time.LocalTime.now() + " S4: " + deltaS4;
//				System.out.println(str);
//				
//				int deltaS5=Math.abs((getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:05") - s5counter));
//				//int s5counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:05");
//				str = " AT " + java.time.LocalTime.now() + " S5: " + deltaS5;
//				System.out.println(str);
//				int deltaS6=Math.abs((getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:06") - s6counter));
//				//int s6counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:06");
//				str = " AT " + java.time.LocalTime.now() + " S6: " + deltaS6;
//				System.out.println(str);
//				
//				float cLC2 = deltaS4 + deltaS5 + deltaS6;
//				System.out.println("Total packetin of C2: "+ cLC2);
//				float deltaLC1=Math.abs(cLC2 - pLC1);
//				float deltaLC2= Math.abs(cLC2 - pLC2);
//				System.out.println("IB is "+ ((cLC1+ cLC2)/2)/(Math.max((cLC1), (cLC2))));
//				System.out.println("AF is: "+( deltaLC1+ deltaLC2)/2);
//				//System.out.println("AF is: "+((deltaS4 + deltaS5 + deltaS6) - LC2));
//				float avgC =(cLC1 + cLC2)/2;
//				System.out.println("Average of C is: "+ avgC);
//				
//				float deltaC1 = Math.abs((cLC1 - avgC));
//				float deltaC2 = Math.abs(cLC2 - avgC);
//				System.out.println(" C1 vs Average: " +deltaC1);
//				System.out.println("C2 vs Average: "+ deltaC2);
//				// Because the value of MAXC1 represents the amount of packetin per second, 
//				// we need to figure out the amount of packetin per second in "getAmountInterval" second
//				double pressureC1= (cLC1*1000/getAmountInterval)/MAXC1;
//				System.out.println("Pressure of C1: "+ pressureC1);
//				double pressureC2= (cLC2*1000/getAmountInterval)/MAXC2;
//				System.out.println("Pressure of C2: "+ pressureC2);
//				pLC1=deltaS1 + deltaS2 + deltaS3;
//				pLC2= deltaS4+ deltaS5 + deltaS6;
//				s1counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:01");
//				s2counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:02");
//				s3counter=getAmountOfPacketIN("10.102.11.249","00:00:00:00:00:00:00:03");
//				s4counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:04");
//				s5counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:05");
//				s6counter=getAmountOfPacketIN("10.102.11.247","00:00:00:00:00:00:00:06");
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
    	}
    }
    
    //Change role function
    public void changeRole (String IPController,String switchID,String newRole) {
		String data = "{\"role\":\"" + newRole+"\"}";
		String url = "http://" + IPController +"/wm/core/switch/"+ switchID+"/role/json";
		
		//cURL Command: curl -X POST -d '{"role":"SLAVE"}' http://10.102.11.249:8080/wm/core/switch/00:00:00:00:00:00:00:01/role/json
		//Expectation value : "00:00:00:00:00:00:00:01":"SLAVE"
		//Equivalent command conversion for Java execution
		String[] command = { "curl", "-d", data, "-X", "POST", url };
		
		ProcessBuilder process = new ProcessBuilder(command);
		Process p;
		try {
			p = process.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			String result = builder.toString();
			logger.info("Role was changed successfully");
			System.out.print(result);

		} catch (IOException e) {
					System.out.print("error");
					e.printStackTrace();
		}    	
    }
    
    //Get and handle Json String
	public int getAmountOfPacketIN(String IPController,String switchID ) throws Exception {    	
    	URL oracle = new URL("http://" + IPController+ ":8080/wm/core/switch/"+ switchID+ "/flow/json");
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    yc.getInputStream()));
        String inputLine="";
        String strJSON= "";
        while ((inputLine = in.readLine()) != null) 
        	strJSON+= inputLine;      	
        in.close();
		JSONObject obj = new JSONObject(strJSON);
		JSONArray flows=obj.getJSONArray("flows");
		//Revieve data from the bottom to get the true value of packetin besides other flows.
		for (int i=flows.length()-1;i>=0;i--) {
			String packetCount= flows.getJSONObject(i).getString("packet_count");
			return Integer.parseInt(packetCount);
		}        
		return 0;
	}
    
    
    
    
    
    
    
    
    
    
    
	/* Test OFMATCH which used for Openflowver10. But this version is 13 :D 
	 * Aim to get the nwtos field in OFMATCH 
//	OFPacketIn pktIn=(OFPacketIn)m;
//	
//	StringBuffer sb =  new StringBuffer("");
//	
//	OFMatch temp=new OFMatch();
//	sb.append("packet_in          [ ");
//    sb.append(sw.getId());
//    sb.append(" -> Controller");
//    sb.append(" ]");
//    sb.append("\nin_port: ");
//    sb.append(pktIn.getData());
//    sb.append("\ntotal length: ");
//    sb.append(pktIn.getTotalLen());
//    
//    sb.append("\ndata_length: ");
//    sb.append(pktIn.getTotalLen() - 18);
//    sb.append("\nbuffer: ");
//    sb.append(pktIn.getBufferId());
 * 
//  Cannot declare this type because its class set was private
//	  OFMatchV1Ver10 temp;
	
	
	
//	byte[] packetData= sb.toString().getBytes();
//	
//	temp.loadFromPacket(packetData, ((short)0xfffc));
//	System.out.println("TOS:" + temp.getNetworkTypeOfService());
//	temp.loadFromPacket(arg0, arg1);
// => Get TOS successfully but its value is 0; 

	/ * 
	It was made for counting packetin and logging to file (Successful) 
	 */
//	String str="\nThe number of packetin at"+java.time.LocalTime.now()+" is:"+ counters.packetIn.getCounterValue()+ "\n";
//	System.out.println(str);
//	logToFile(str);
	
//	Test to run threads periodically, but it failed     
//	myTimer.scheduleAtFixedRate(new TimerTask() {  
//		@Override
//		public void run () {
//			System.out.println("The number of packetin is:"+ counters.packetIn.getCounterValue());
//		}
//	},1000, 1000);
	
	//Handle Message by String
	/*
	 *     protected class PrintCounter implements Runnable {
    	
    	public void run () {

    		
    		//sendData();
    		
    		try {
    			System.out.println("Started at"+ java.time.LocalTime.now());
//    			conn = new ZooKeeperConnection();
//    			zk = conn.connect("10.102.11.226");
    			String str="" ;
    			//Load of C2
    			String messC2 = getDataFromZK ("C2Znode","Mess");
    			System.out.println(messC2); 
    			//Handle String with format like this [cLC1=10, S3=5, S2=4, S1=1]
    			String [] setC2= messC2.replaceAll("[\\[\\]\\s]", "").split(","); 
    			
    			// Handle String with the format like this {cLC1=10, S3=5, S2=4, S1=1}
    			
    			//setC2[0].substring(1);
//    			ArrayList <Long> setC2 = new ArrayList <Long>();
//    			setC2.add(Long.parseLong(items[0]));
//    			setC2.add(Long.parseLong(items[1]));
//    			setC2.add(Long.parseLong(items[2]));
//    			long cS4Counter= Long.parseLong(setC2[0]);
//    			long cS5Counter= Long.parseLong(setC2[1]);
//    			long cS6Counter= Long.parseLong(setC2[2]); 
    			
     			long cLC2 = Long.parseLong(setC2[0].substring(5));
    			str="The number of C2 at "+java.time.LocalTime.now()+" is: " + cLC2;    			
    			System.out.println(str); 
    			float prC2 = (float)cLC2/(MAXC2);
    			System.out.println("Pressure of C2 " + prC2);
    			float deltaLC2= Math.abs(cLC2 -pLC2);
    			
    			//Load of C3
    			String messC3 = getDataFromZK ("C3Znode","Mess");
    			String [] setC3= messC3.replaceAll("[\\[\\]\\s]", "").split(","); 
    			long cS7Counter= Long.parseLong(setC3[0]);
    			long cS8Counter= Long.parseLong(setC3[1]);
    			long cS9Counter= Long.parseLong(setC3[2]);    			
     			long cLC3 = cS7Counter + cS8Counter + cS9Counter;
    			str="The number of C3 at "+java.time.LocalTime.now()+" is: " + cLC3;    			
    			System.out.println(str); 
    			float prC3 = (float)cLC3/(MAXC3);
    			System.out.println("Pressure of C3 " + prC3);
    			float deltaLC3=Math.abs( cLC3 -pLC3);
    			
    			//Load of C4
    			String messC4 = getDataFromZK ("C4Znode","Mess");
    			String [] setC4= messC4.replaceAll("[\\[\\]\\s]", "").split(","); 
    			long cS10Counter= Long.parseLong(setC4[0]);
    			long cS11Counter= Long.parseLong(setC4[1]);
    			long cS12Counter= Long.parseLong(setC4[2]);    			
     			long cLC4 = cS10Counter + cS11Counter + cS12Counter;
    			str="The number of C4 at "+java.time.LocalTime.now()+" is: " + cLC4;    			
    			System.out.println(str); 
    			float prC4 = (float)cLC4/(MAXC4);
    			System.out.println("Pressure of C4 " + prC4);    
    			float deltaLC4= Math.abs(cLC4 -pLC4);
    			
    			Comparator c=Collections.reverseOrder();
    			// Load of C1
    			ArrayList<Long> setC1 = new ArrayList <Long>();
    			long cS1Counter=counters.packetInS1.getCounterValue();
    			long cS2Counter= counters.packetInS2.getCounterValue();
    			long cS3Counter= counters.packetInS3.getCounterValue();
    			setC1.add(cS1Counter);
    			setC1.add(cS2Counter);
    			setC1.add(cS3Counter);    			
    			//Collections.sort(setC1,c);
    			
    			System.out.println(setC1);
    			long cLC1 = cS1Counter + cS2Counter + cS3Counter;
    			str="The number of C1 at  "+java.time.LocalTime.now()+" is: " + cLC1;
    			System.out.println(str); 
    			float prC1 = cLC1/(MAXC1);
    			System.out.println("Pressure of C1 " + prC1);
    			float deltaLC1= Math.abs( cLC1 -pLC1);
    			
    			// Total Load of controllers
    			float totalLoad=cLC1+cLC2+cLC3+cLC4;
    			
    			// Calculate global imbalanced degree 
    			float IB= (totalLoad)/(4* Math.max(Math.max(cLC1, cLC2),Math.max(cLC3, cLC4)));
    			System.out.println("Imbalance degree: " + IB);
    			
    			//Calculate the average of Controller load
    			float avgC=totalLoad/4;
    			System.out.println("The average of Controller: " + avgC);
    			
    			//The difference between current load vs average load 
    			float deltaAvgC1 =Math.abs(cLC1 -avgC);
    			float deltaAvgC2 = Math.abs(cLC2 - avgC);
    			float deltaAvgC3 = Math.abs(cLC3 - avgC);
    			float deltaAvgC4 = Math.abs(cLC4 - avgC);
    			System.out.println("Delta average: " + deltaAvgC1 + " ; "+ deltaAvgC2 + " ; "+ deltaAvgC3 + " ; "+ deltaAvgC4);
    			
    			//Calculate the average of load fluctuation
    			float AF = (deltaLC1+deltaLC2+deltaLC3+deltaLC4)/4;
    			System.out.println("Average of load fluctuation: " + AF);
    			
    			
    			// Save the previous value of LOAD
    			pLC1=cLC1;
    			pLC2=cLC2;
    			pLC3=cLC3;
    			pLC4=cLC4;
    			
    			//Change role
//    			float maxPrC=Math.max(Math.max(prC1, prC2), Math.max(prC3, prC4));    			
//    			if (prC1 ==maxPrC) {
//    				long maxSw=Math.max(Math.max(cS1Counter, cS2Counter), cS3Counter);
//    				
//    			}
    			// reset to count from 0
    			counters.packetInS1.reset();
    			counters.packetInS2.reset();
    			counters.packetInS3.reset();
    			
    			

    			System.out.println("Ended at"+ java.time.LocalTime.now());
    		}catch (Exception ex) {
    			System.out.println("FAILED");
    		}
        	//counters.packetIn.reset();
    	}
    }
	 * 
	 * 
	 * ***/
}
