package net.floodlightcontroller.mactracker;
 
import java.util.ArrayList;
import java.util.List;
 
import net.floodlightcontroller.core.types.SwitchMessagePair;
 
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
 
public class MACTrackerResource extends ServerResource {
    @Get("json")
    public List<SwitchMessagePair> retrieve() {
        MACTrackerService pihr = (MACTrackerService)getContext().getAttributes().get(MACTrackerService.class.getCanonicalName());
        List<SwitchMessagePair> l = new ArrayList<SwitchMessagePair>();
        l.addAll(java.util.Arrays.asList(pihr.getBuffer().snapshot()));
        return l;
    }
}