package net.floodlightcontroller.mactracker;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.*;
import org.projectfloodlight.openflow.protocol.OFMessage;
import org.projectfloodlight.openflow.protocol.OFType;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.HARole;
import net.floodlightcontroller.core.IControllerCompletionListener;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.RoleInfo;
import net.floodlightcontroller.core.internal.Controller.IUpdate;
import net.floodlightcontroller.core.internal.Controller.ModuleLoaderState;
import net.floodlightcontroller.core.internal.RoleManager;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.core.types.SwitchMessagePair;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IHAListener;
import net.floodlightcontroller.core.IInfoProvider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.Set;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.pktinhistory.IPktinHistoryService;
import net.floodlightcontroller.pktinhistory.PktInHistoryWebRoutable;
import net.floodlightcontroller.restserver.IRestApiService;
import net.floodlightcontroller.storage.IStorageSourceListener;
import net.floodlightcontroller.util.ConcurrentCircularBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class MACTracker implements IOFMessageListener, MACTrackerService,IFloodlightModule ,IFloodlightProviderService, IStorageSourceListener, IInfoProvider{
	
	protected IFloodlightProviderService floodlightProvider;
	protected ConcurrentCircularBuffer<SwitchMessagePair> buffer;
	protected IRestApiService restApi;
	protected Set<Long> macAddresses;
	protected static Logger logger;
	protected static int counter;
	protected static int counter1;
	
	
	public ConcurrentCircularBuffer<SwitchMessagePair> getBuffer() {
	    return buffer;
	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return MACTracker.class.getSimpleName();
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
	    Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
	    l.add(MACTrackerService.class);
	    return l;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
	    Map<Class<? extends IFloodlightService>, IFloodlightService> m = new HashMap<Class<? extends IFloodlightService>, IFloodlightService>();
	    m.put(MACTrackerService.class, this);
		return m;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		// TODO Auto-generated method stub
		Collection<Class<? extends IFloodlightService>> l =
		        new ArrayList<Class<? extends IFloodlightService>>();
		    l.add(IFloodlightProviderService.class);
		    l.add(MACTrackerService.class);
		    l.add(IRestApiService.class);
		    
		return l;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		// TODO Auto-generated method stub
	    floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
	    macAddresses = new ConcurrentSkipListSet<Long>();
	    logger = LoggerFactory.getLogger(MACTracker.class);
	    IRestApiService restApiService = context.getServiceImpl(IRestApiService.class);
	    counter=0;
	    counter1=0;
		restApi = context.getServiceImpl(IRestApiService.class);
	    buffer = new ConcurrentCircularBuffer<SwitchMessagePair>(SwitchMessagePair.class, 100);
	}

	@Override
	public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
		// TODO Auto-generated method stub
		floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
		restApi.addRestletRoutable(new MACTrackerWebRoutable());
	}
	static public int getCounter() {
		return counter;
	}

	
	@Override
	public Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
		
		// TODO Auto-generated method stub
		//logger.info("Start cURLs.....................");
		switch(msg.getType()) {
		case PACKET_IN:
			//System.out.println("From MACTracker" + msg);
			counter++;
			//System.out.println("at MACTracker "+java.time.LocalTime.now()+ "is: "+ counter);
			//buffer.add(new SwitchMessagePair(sw, msg));
			break;
		default:
			break;
		}
		
		//logger.info("The amount of PACKETIN {} from MACTracker is ", counter);

        Ethernet eth =
                IFloodlightProviderService.bcStore.get(cntx,
                                            IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
 
        Long sourceMACHash = eth.getSourceMACAddress().getLong();
        if (!macAddresses.contains(sourceMACHash)) {
            macAddresses.add(sourceMACHash);
            logger.info("MAC Address: {} seen on switch: {}",
                    eth.getSourceMACAddress().toString(),
                    sw.getId().toString());
        }
		int totalCount=0;
//		logger.info("Start cURLs");
		
//        try {
//        		getAmountOfPacketIN("00:00:00:00:00:00:00:01");
//                //totalCount+=getAmountOfPacketIN("00:00:00:00:00:00:00:01")+getAmountOfPacketIN("00:00:00:00:00:00:00:02")+getAmountOfPacketIN("00:00:00:00:00:00:00:03");
//                //System.out.println("The total packetin is "+getAmountOfPacketIN("00:00:00:00:00:00:00:01"));
//            } catch (Exception e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//            }

		return Command.CONTINUE;
	}
	
	public void getAmountOfPacketIN(String switchID ) throws Exception {
    	logger.info("Start cURL");
    	URL oracle = new URL("http://10.102.11.247:8080/wm/core/switch/"+ switchID+ "/flow/json");
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    yc.getInputStream()));
        String inputLine="";
        String strJSON= "";
        while ((inputLine = in.readLine()) != null) 
            //System.out.println(inputLine);
        	strJSON+= inputLine;      	
        	//logger.info("JSON: {}",inputLine);

//        	String newPacketin = obj.getJSONObject("flows").getString("packet_count");
//        	logger.info("Number of PACKEIN from s1 {}", newPacketin);
        in.close();
        //System.out.println(strJSON);
		JSONObject obj = new JSONObject(strJSON);
		JSONArray flows=obj.getJSONArray("flows");
		for (int i=0;i<flows.length();i++) {
			String packetcount= flows.getJSONObject(i).getString("packet_count");
			//return packetcount;
			System.out.println("The amount of packetin is:"+ packetcount);
		}        
//        JSONObject obj = new JSONObject(strJSON);
//        String newPacketin = obj.getJSONObject("flows").getString("packet_count");
//        logger.info("Number of PACKEIN from s1 {}", newPacketin);
		//return null;
   
	}
//	public void readJSON () throws Exception {
//		
//	}
//	net.floodlightcontroller.core.internal.OFSwitchManager.switchesInitialState={"00:00:00:00:00:00:00:01":"ROLE_MASTER","00:00:00:00:00:00:00:02":"ROLE_MASTER", "00:00:00:00:00:00:00:03":"ROLE_MASTER"}
//	net.floodlightcontroller.core.internal.OFSwitchManager.switchesInitialState={ "00:00:00:00:00:00:00:04":"ROLE_SLAVE","00:00:00:00:00:00:00:05":"ROLE_SLAVE","00:00:00:00:00:00:00:06":"ROLE_SLAVE"}


	@Override
	public Map<String, Object> getInfo(String type) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void rowsModified(String tableName, Set<Object> rowKeys) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void rowsDeleted(String tableName, Set<Object> rowKeys) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addOFMessageListener(OFType type, IOFMessageListener listener) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeOFMessageListener(OFType type, IOFMessageListener listener) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Map<OFType, List<IOFMessageListener>> getListeners() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public HARole getRole() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public RoleInfo getRoleInfo() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, String> getControllerNodeIPs() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getControllerId() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setRole(HARole role, String changeDescription) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addUpdateToQueue(IUpdate update) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addHAListener(IHAListener listener) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeHAListener(IHAListener listener) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void handleOutgoingMessage(IOFSwitch sw, OFMessage m) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addInfoProvider(String type, IInfoProvider provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeInfoProvider(String type, IInfoProvider provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Map<String, Object> getControllerInfo(String type) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public long getSystemStartTime() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public Map<String, Long> getMemory() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Long getUptime() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void handleMessage(IOFSwitch sw, OFMessage m, FloodlightContext bContext) {
		// TODO Auto-generated method stub
		switch(m.getType()) {
		case PACKET_IN:
			//System.out.println("From MACTracker" + msg);
			counter1++;
			System.out.println("at MACTracker Handle "+java.time.LocalTime.now()+ "is: "+ counter1);
			//buffer.add(new SwitchMessagePair(sw, msg));
			break;
		default:
			break;
		}
	}


	@Override
	public RoleManager getRoleManager() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ModuleLoaderState getModuleLoaderState() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addCompletionListener(IControllerCompletionListener listener) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void removeCompletionListener(IControllerCompletionListener listener) {
		// TODO Auto-generated method stub
		
	}
}
//{"nodeId": 3, "domainId": 1, "hostname": "192.168.56.1", "port": 6644},\
//{"nodeId": 4, "domainId": 1, "hostname": "192.168.56.1", "port": 6645}\
