package net.floodlightcontroller.mactracker;

import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.core.types.SwitchMessagePair;
import net.floodlightcontroller.util.ConcurrentCircularBuffer;

public interface MACTrackerService extends IFloodlightService {
	public ConcurrentCircularBuffer<SwitchMessagePair> getBuffer();

}
